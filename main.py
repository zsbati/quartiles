n = int(input().strip())
string_numbers = input().strip()
numbers = string_numbers.split(" ")
nums = [int(stri) for stri in numbers]
nums.sort()

median = 0
index = 0
if n%2 == 0:
  index = int(n/2) - 1 #counting from 0
  median = (nums[index] + nums[index+1])/2
else:
  index = int(n/2)
  median = nums[index]
  
#print("median= "+str(median))

#first quartile: middle between the median and the lowest number
# find the index, where this would be
first_quart = 0.0
third_quart = 0.0
high_index = 0
if n%2 == 0:
  high_index = int(n/2) - 1
else:
  high_index = int(n/2) - 1
if high_index%2 == 0:
  index = int(high_index/2)
  first_quart = nums[index]
  third_quart = nums[n-1-index]
else:
  index = int(high_index/2)
  first_quart = (float(nums[index]) + float(nums[index+1]))/(float(2))
  third_quart = (float(nums[n-1-index]) + float(nums[n-2-index]))/(float(2))

print(first_quart) 
print(median) 
print(third_quart)
